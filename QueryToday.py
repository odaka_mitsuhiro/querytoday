#!/usr/bin/env python
# coding: utf-8

import wikipedia
import time
import datetime

wikipedia.set_lang("ja")

d=datetime.datetime.now(datetime.timezone(datetime.timedelta(hours=9)))

wkd=d.weekday()
if wkd==0:
    wkd_=u"（月）"
elif wkd==1:
    wkd_=u"（火）"
elif wkd==2:
    wkd_=u"（水）"
elif wkd==3:
    wkd_=u"（木）"
elif wkd==4:
    wkd_=u"（金）"
else:
    wkd_=""

wp=wikipedia.page(str(d.month)+u"月"+str(d.day)+u"日").content.split("\n")

wp=wp[wp.index("== 記念日・年中行事 =="):wp.index("=== 誕生日（フィクション） ===")]

print(u"今日は"+str(d.month)+u"月"+str(d.day)+u"日"+wkd_+"です。")
for i in range(len(wp)):
    if wp[i]!="":
        print(wp[i])

if wkd==4:
    d+=datetime.timedelta(days=3)
    print("\n"+u"ちなみに"+str(d.month)+u"月"+str(d.day)+u"日（月）は、")
else:
    d+=datetime.timedelta(days=1)
    wkd+=1
    if wkd==1:
        wkd_=u"（火）"
    elif wkd==2:
        wkd_=u"（水）"
    elif wkd==3:
        wkd_=u"（木）"
    elif wkd==4:
        wkd_=u"（金）"
    else:
        wkd_=""
    print("\n"+u"ちなみに"+str(d.month)+u"月"+str(d.day)+u"日"+wkd_+"は、")

wp=wikipedia.page(str(d.month)+u"月"+str(d.day)+u"日").content.split("\n")
wp=wp[wp.index("== 記念日・年中行事 =="):wp.index("=== 誕生日（フィクション） ===")]
for i in range(len(wp)):
    if wp[i]!="":
        print(wp[i])
